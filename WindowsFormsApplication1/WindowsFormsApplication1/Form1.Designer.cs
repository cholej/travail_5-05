﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbAfficher = new System.Windows.Forms.TextBox();
            this.cbCouleur = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbAfficher
            // 
            this.tbAfficher.Location = new System.Drawing.Point(134, 132);
            this.tbAfficher.Multiline = true;
            this.tbAfficher.Name = "tbAfficher";
            this.tbAfficher.Size = new System.Drawing.Size(180, 88);
            this.tbAfficher.TabIndex = 0;
            this.tbAfficher.TextChanged += new System.EventHandler(this.tbAfficher_TextChanged);
            // 
            // cbCouleur
            // 
            this.cbCouleur.FormattingEnabled = true;
            this.cbCouleur.Location = new System.Drawing.Point(193, 51);
            this.cbCouleur.Name = "cbCouleur";
            this.cbCouleur.Size = new System.Drawing.Size(121, 21);
            this.cbCouleur.TabIndex = 1;
            this.cbCouleur.SelectedIndexChanged += new System.EventHandler(this.cbCouleur_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Choix couleur :";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 262);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbCouleur);
            this.Controls.Add(this.tbAfficher);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbAfficher;
        private System.Windows.Forms.ComboBox cbCouleur;
        private System.Windows.Forms.Label label1;
    }
}

