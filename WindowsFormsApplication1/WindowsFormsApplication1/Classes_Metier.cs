﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public class Cmouton
    {
        public string id_mouton { get; set; }
        public string couleur { get; set; }
        public string poids { get; set; }


        public Cmouton(string sid_mouton, string scouleur, string spoids)
        {
            id_mouton = sid_mouton;
            couleur = scouleur;
            poids = spoids;

        }

    }
    public class Cmoutons
    {
        public Dictionary<string, Cmouton> ocollDicoMouton;
       
        private string connectionString = "SERVER=127.0.0.1; DATABASE=gestiontroupeau; UID=root; PASSWORD=";


        public MySqlDataReader getReader(string squery)
        {
            MySqlConnection ocnx = new MySqlConnection(connectionString);
            ocnx.Open();
            MySqlCommand ocmd = new MySqlCommand(squery, ocnx);
            MySqlDataReader ord = ocmd.ExecuteReader();
            return ord;
        }
        private Cmoutons()
        {

            ocollDicoMouton = new Dictionary<string, Cmouton>();
            
           
            string query = "SELECT * FROM mouton";
            MySqlDataReader ord = getReader(query);

            while (ord.Read())
            {
                Cmouton omouton = new Cmouton(Convert.ToString(ord["id"]), Convert.ToString(ord["couleur"]), Convert.ToString(ord["poids"]));

                ocollDicoMouton.Add(Convert.ToString(ord["id"]), omouton);
            }
        

        }

       /* public List<Cmouton> getMoutonByColor(String couleurMouton)
             {
                 List<Cmouton> liste = new List<Cmouton>();
                 foreach (string entrer in ocollDicoMouton.Keys)
                 {
                     Cmouton omouton = ocollDicoMouton[entrer];
                     if (omouton.getcouleurMouton()==couleurMouton)
                     {
                         liste.Add(omouton);
                     }
                 }
                 return liste;
             } 
        */
    }
        
    }
